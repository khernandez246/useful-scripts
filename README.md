![alt text](https://i.imgur.com/Dn2gvkI.png "Ship NSD Logo")
# Useful Scripts
-------------------------------------------

- [SQL Queries](#table-of-contents)
    + [SQL Query to fetch orders for NSD App](#sql-query-to-fetch-orders-for-nsd-app)
    + [SQL Query for fetching bill numbers by interliner Id](#sql-query-for-fetching-bill-numbers-by-interliner-id)
    + [SQL Query for updating product line for NSD App](#sql-query-for-updating-product-line-for-nsd-app)
    + [SQL Query to search orders with more than 1 product](#sql-query-to-search-orders-with-more-than-1-product)
- Python Scripts
- Misc.
    + [Linux command to copy files from PC to Linux server](#linux-command-to-copy-files-from-pc-to-linux-server)


-------------------------------------------


### SQL Query to fetch orders for NSD App

```
WITH R AS(
  SELECT
    DETAIL_NUMBER,
    CASE
      WHEN TRACE_TYPE = 'N' THEN TRACE_NUMBER
      ELSE NULL
    END AS CARRIER_NAME,
    CASE
      WHEN TRACE_TYPE = 'Y' THEN TRACE_NUMBER
      ELSE NULL
    END AS CARRIER_PRO,
    CASE
      WHEN TRACE_TYPE = 'P' THEN TRACE_NUMBER
      ELSE NULL
    END AS PO
  FROM
    TRACE
)
SELECT
  T.BILL_NUMBER,
  T.DETAIL_LINE_ID,
  TL.DESCRIPTION,
  TL.SEQUENCE,
  T.CALLNAME,
  T.PIECES,
  T.WEIGHT,
  T.DESTADDR1 || ', ' || T.DESTCITY || ', ' || T.DESTPROV || ' ' || T.DESTPC AS DESTADDR3,
  T.DESTNAME AS CONSIGNEE_NAME,
  MAX(R.CARRIER_NAME) AS CARRIER_NAME,
  MAX(R.CARRIER_PRO) AS CARRIER_PRO,
  LISTAGG(DISTINCT R.PO, ' / ') AS PO,
  T.SERVICE_LEVEL,
  LINE.INTERLINER_ID,
  CASE
    WHEN TL.FIELD_24 IS NOT NULL THEN 'RECEIVED'
    WHEN TL.FIELD_25 IS NOT NULL THEN 'EXCEPTION'
    ELSE NULL
  END AS PRODUCT_STATUS
FROM
  TLORDER AS T
  LEFT OUTER JOIN TRACE AS TR ON T.DETAIL_LINE_ID = TR.DETAIL_NUMBER
  LEFT OUTER JOIN R ON T.DETAIL_LINE_ID = R.DETAIL_NUMBER
  LEFT OUTER JOIN TLDTL AS TL ON T.DETAIL_LINE_ID = TL.ORDER_ID
  LEFT OUTER JOIN ORDER_INTERLINER AS LINE ON T.DETAIL_LINE_ID = LINE.DETAIL_LINE_ID
WHERE
  LINE.INTERLINER_ID = 'NSDIAD'
  AND (
    TR.TRACE_NUMBER = '10018195'
    OR T.BILL_NUMBER = '10018195'
  )
GROUP BY
  T.BILL_NUMBER,
  TL.DESCRIPTION,
  T.CALLNAME,
  T.DESTCITY,
  T.DESTPC,
  T.DESTNAME,
  T.DESTADDR1,
  T.SERVICE_LEVEL,
  LINE.INTERLINER_ID,
  TL.FIELD_24,
  TL.FIELD_25,
  T.DESTNAME,
  T.PIECES,
  T.WEIGHT,
  T.DESTPROV,
  T.DETAIL_LINE_ID,
  TL.SEQUENCE 
```
------------
### SQL Query for fetching bill numbers by interliner Id

```
SELECT
  BILL_NUMBER
FROM
  TLORDER AS T
  LEFT OUTER JOIN ORDER_INTERLINER AS LINE ON T.DETAIL_LINE_ID = LINE.DETAIL_LINE_ID
WHERE
  LINE.INTERLINER_ID = 'NSDIAD'
FETCH FIRST
  50 ROWS ONLY
```
---------------
### SQL Query for updating product line for NSD App

```
UPDATE
  TLDTL
SET
  FIELD_24 = CASE
    WHEN '' = 'true' THEN CURRENT TIMESTAMP
    ELSE NULL
  END,
  FIELD_25 = CASE
    WHEN '' = 'true' THEN CURRENT TIMESTAMP
    ELSE NULL
  END
WHERE
  ORDER_ID = ''
  AND SEQUENCE = ''
```

-----------
### SQL Query to search orders with more than 1 product
```
SELECT
  DISTINCT BILL_NUMBER,
  INTERLINER_ID
FROM
  TLORDER AS T
  LEFT OUTER JOIN ORDER_INTERLINER AS LINE ON T.DETAIL_LINE_ID = LINE.DETAIL_LINE_ID
WHERE
  T.DETAIL_LINE_ID IN (
    SELECT
      ORDER_ID
    FROM
      TLDTL
    GROUP BY
      ORDER_ID
    HAVING
      COUNT(SEQUENCE) >= 2
  )
  AND LINE.INTERLINER_ID = 'CROSSBNA'
FETCH FIRST
  25 ROWS ONLY
```
-----------
### Linux command to copy files from PC to Linux server
`scp -r /path/to/local/folder/* username@server_url_here:/path/to/folder`